#include <thread>
#include <future>
#include <cstdio>
#include <chrono>
#include <iostream>
#include <windows.h>

const int NumItems = 10000000;

void incrementer(int *ptr, std::mutex *lock)
{
	for (;;)
	{
		std::lock_guard<std::mutex> guard(*lock);
		int val = *ptr;
		// Are we done yet?
		if (val == NumItems)
			return;
		*ptr = val + 1;
	}
}

void test_std_mutex(size_t num_workers)
{
	std::vector< std::future<void> > futures;
	std::mutex lock;
	int shared_int = 0;

	for (size_t i = 0; i < num_workers; i++)
	{
		futures.emplace_back(std::async(std::launch::async, incrementer, &shared_int, &lock));
	}

	for (auto &fut : futures)
	{
		fut.get();
	}
}

void incrementer_win(int *ptr, HANDLE lock)
{
	for (;;)
	{
		DWORD dwWaitResult;
		do {
			dwWaitResult = WaitForSingleObject(lock, INFINITE);
		} while (dwWaitResult != WAIT_OBJECT_0);

		int val = *ptr;
		// Are we done yet?
		if (val == NumItems)
		{
			ReleaseMutex(lock);
			return;
		}
		*ptr = val + 1;

		ReleaseMutex(lock);
	}
}

void test_win_mutex(size_t num_workers)
{
	std::vector< std::future<void> > futures;
	auto lock = CreateMutex(NULL, FALSE, NULL);
	int shared_int = 0;

	for (size_t i = 0; i < num_workers; i++)
	{
		futures.emplace_back(std::async(std::launch::async, incrementer_win, &shared_int, lock));
	}

	for (auto &fut : futures)
	{
		fut.get();
	}
}


int main()
{
	using namespace std::chrono;
	for (size_t num_workers = 1; num_workers <= 8; num_workers *= 2)
	{
		auto t0 = steady_clock::now();
		test_std_mutex(num_workers);
		auto t1 = steady_clock::now();
		test_win_mutex(num_workers);
		auto t2 = steady_clock::now();
		auto d1 = t1 - t0;
		auto d2 = t2 - t1;

		std::cout << "workers: " << num_workers << " std::mutex:" << duration<double, std::milli>(d1).count() << " ms" << std::endl;
		std::cout << "workers: " << num_workers << " Windows mutex:" << duration<double, std::milli>(d2).count() << " ms" << std::endl;
	}
	return 0;
}
